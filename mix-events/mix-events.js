   

var mixEvents = function (obj) {
  var storage = {};

  obj.on = function (eventType, callback) {
    if (arguments.length < 2) {
      throw new Error('Not enough arguments');
    }

    
    storage[eventType] = storage[eventType] || [];
    storage[eventType].push(callback);
  };

  obj.trigger = function (eventType) {
    if (eventType === void 0) { return; }

    var args = Array.prototype.slice.call(arguments, 1);
    var listOfCallbacks = storage[eventType] || [];

    listOfCallbacks.forEach(function (cb) {
      cb.apply(obj, args);
    });
  };

  obj.remove = function (eventType, callback) {

    storage[eventType] = storage[eventType] || [];
    if (arguments.length >= 2) {
      var i = 0;
      while (i < storage[eventType].length) {
        if (storage[eventType][i] === callback) {
          storage[eventType].splice(i, 1);
        } else {
          i += 1;
        }
      }
    } else {
      
      delete storage[eventType];
    }
  };

  return obj;
};
