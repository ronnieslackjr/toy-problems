var should = chai.should();

describe('coinSums', function() {
  it('should exist', function(){
    should.exist(coinSums);
  });

  it('should be a function', function() {
    coinSums.should.be.a.Function;
  });

  it('should return a number', function() {
    var result = coinSums(14);
    should.exist(result);
    result.should.be.an.instanceof(Number);
  });


  it('should do the thing', function(){
    var result = coinSums(1);
    result.should.be.eql(1);
  });

    it('should do the damn thing', function(){
    var result = coinSums(17);
    result.should.be.eql(28);
  });


  });

