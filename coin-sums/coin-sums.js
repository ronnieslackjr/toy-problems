// var coinSums = function(total){
//   //Input: A number, the total value of coins to make change from
//   //Output: The number of different ways to make change from total
//   //Constraints: ??
//   //Edge cases:??
//     var count = 0;
//   //Start at 0
//   var coins = [1, 2, 5, 10, 20, 50, 100, 200];

//   var makeChange = function(index, value){
//   //helper function to actually recurse
//     //Do we need this, or does my algorithm suck?
//     var curr = coins[index];
//     //set variable for coins at current index
//       //we will count down
//     if(index === 0){
//       //index 0 is always going to be 1
//         if(value % curr === 0){
//         //if we've reached 1, we know we have a match
//         count++;
//         //increment counter
//       }
//       return;
//     }
//     while( value >= 0 ){
//         //Until value is less than 0
//       makeChange(index-1, value);
//       //recurse, going backwards through coins
//       value -= curr;
//     }
//   }
//   makeChange(coins.length-1, total);
//   //Don't forget to call the function!
//   //And return count;
//   return count;
// };

function coinSums (total) {
  var coins = [1,2,5,10,20,50,100,200];
  var count = 0;

  //recursive function which checks all possible sums
  var checkSum = function(sum, index) {
    if (sum === total) {
      //increment the count if the sum reaches the total
      count++;
    } else if (sum < total) {
      //if the sum is less than the total
      for (var i = index; i < coins.length; i++) {
      //call the function recursively for all possible combinations
        checkSum(sum+coins[i], i);
      }
    } 
  };
 //start sum and index at 0 to begin loop
  checkSum(0,0);
  return count;
}

console.log(coinSums(1))
console.log(coinSums(17))