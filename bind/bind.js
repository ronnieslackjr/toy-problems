
var bind = function(func, context) {
  let curriedArgs = Array.from(arguments).slice(2);
  return function(...args) {
    return func.apply(context, concat(curriedArgs, args));
  }
};

Function.prototype.bind = Function.prototype.bind || function(context) {
  return function(...args) {
    this.apply(context, args);
  }
};