var rotateMatrix = function(matrix) {
  //If only one item in the matrix, return it
  if (matrix.length === 1) {
    return matrix
  }
  
  //Just reverse it to start so we don't have to later
  matrix = matrix.reverse();
  for (var i = 0; i < matrix.length; i++) {
    // loop through once, these will be our rows
    for (var j = 0; j < i; j++) {
      // second loop is through the columns
      var temp = matrix[i][j];
      matrix[i][j] = matrix[j][i];
      matrix[j][i] = temp;
}
  }
  return matrix
}

console.log(rotateMatrix([ [ 1, 2, 3, 4 ], [ 5, 6, 7, 8 ], [ 9, "A", "B", "C" ], [ "D", "E", "F", "G" ] ]))
console.log(rotateMatrix([1]))