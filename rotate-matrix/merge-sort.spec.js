var should = chai.should();

describe('rotateMatrix', function() {
  it('should exist', function(){
    should.exist(rotateMatrix);
  });

  it('should be a function', function() {
    rotateMatrix.should.be.a.Function;
  });

  it('should return an array', function() {
    var result = rotateMatrix([1]);
    should.exist(result);
    result.should.be.an.instanceof(Array);
  });

    it('should rotate an array', function() {
    const  actual = rotateMatrix([ [ 1, 2, 3, 4 ], [ 5, 6, 7, 8 ], [ 9, "A", "B", "C" ], [ "D", "E", "F", "G" ] ]);
    const expected = [ [ "D", 9, 5, 1 ], [ "E", "A", 6, 2 ], [ "F", "B", 7, 3 ], [ "G", "C", 8, 4 ] ]
    expect(actual).to.equal(expected);
  });


});

