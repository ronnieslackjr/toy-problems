var shuffleDeck = function(deck, count = 0){
    if (count > 1) {
        return deck
    }
    //Input: An array, a deck of any size
    //Ouput: The same deck, with elements ordered randomly
    //Constraints: Should not have bias based on the size of deck
    //Edge Cases:??
    //let currCard;
    //let randomCard;
    //Loop through array
    deck.forEach(function(card, index){
  //At each index
      //Grab current card, 
        //replace with filler to hold position
        //Generate a random index for it
       let currCard = card;
       let randomIndex = Math.floor(Math.random() * deck.length)
       let randomCard = deck[randomIndex];
       deck[index] = randomCard;
       deck[randomIndex] = card;
    })
    shuffleDeck(deck, ++count);
    return deck;
      
}