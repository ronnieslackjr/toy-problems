function mergeSort(arr){
//Input: An array containing any number of numbers
//Output: An array sorted from largest to smallest
//Constaints: Cannot use array.sort
//Edge Cases:??
//If array only has 1 elements or left, return it    
if (arr.length === 1) {
        return arr;
    }
    //Divide up the array
      //find middle point
      //top half is from 0 index to middle
      //bottom half is middle index to end
          //slice(I: an array, a begin index, and an end index 
        //O:A shallow copy of elements in array from start to end(non inclusive)  
        //C: ?? E:??)
    let mid = parseInt(arr.length / 2)
    let top = arr.slice(0, mid)
    let bottom = arr.slice(mid, arr.length);

    

    function merge(top, bottom) {
        //make empty aray to hold results
        let result = [];
        while (top.length && bottom.length) {
            //while both halves of the array have something in them
              //check value of arrays at 0 index
              //if top's value is smaller, push it to result
                //push it and pop it off of array with shift
              //if bottom's valule is smaller
                //push it to result and shift it off array
            (top[0] <= bottom[0]) ? result.push(top.shift()) : result.push(bottom.shift());
        }
      
    while (top.length){
        //if top has length but bottom does not
          //shoud only have 1 more value
          //push it to result
        result.push(top.shift())
    }
    while (bottom.length) {
        //if bottom has lenght, but top does not
        //should only have one value
        //push it to result
        result.push(bottom.shift())
    }    
      //don't forget to return your result;  
      return result;

    
    
}
    //return merge function, with mergesort done to top and bottom
    return merge(mergeSort(top), mergeSort(bottom))
}

//Start unsorted
//Split in half
  //Kep splitting