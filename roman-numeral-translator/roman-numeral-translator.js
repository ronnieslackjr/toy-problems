var DIGIT_VALUES = {
  I: 1,
  V: 5,
  X: 10,
  L: 50,
  C: 100,
  D: 500,
  M: 1000
};
function translateRomanNumeral (romanNumeral) {
    romanNumeral = romanNumeral.toUpperCase();
    var result = 0;
    for (var i = romanNumeral.length - 1; i >= 0; i--) {
      var digit = romanNumeral[i], prev = romanNumeral[i + 1];
      if (!DIGIT_VALUES[digit]) return "null";
      if (DIGIT_VALUES[digit] < DIGIT_VALUES[prev]) {
        result -= DIGIT_VALUES[digit];
      } else {
        result += DIGIT_VALUES[romanNumeral[i]];
      }
    }
    return result;  
}
  
  console.log(translateRomanNumeral('LI'))
