var should = chai.should();

describe('translateRomanNumeral', function() {
  it('should exist', function(){
    should.exist(translateRomanNumeral);
  });

  it('should be a function', function() {
    translateRomanNumeral.should.be.a.Function;
  });

//   it('should return a number', function() {
//     var result = translateRomanNumeral('CM');
//     should.exist(result);
//     result.should.be.an.instanceof(Number);
//   });


  it('should do the thing', function(){
    var result = translateRomanNumeral('LX');
    result.should.be.eql(60);
  });

    it('should do the damn thing', function(){
    var result = translateRomanNumeral('IV');
    result.should.be.eql(4);
  });
  
      it('should do the thing again', function(){
    var result = translateRomanNumeral('horse');
    result.should.be.eql(null);
});
        it('should do the thing still', function(){
    var result = translateRomanNumeral('');
    result.should.be.eql(0);
  
  });
  });

