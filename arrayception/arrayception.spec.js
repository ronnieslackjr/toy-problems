var should = chai.should();

describe('arrayception', function() {
  it('should exist', function(){
    should.exist(arrayception);
  });

  it('should be a function', function() {
    arrayception.should.be.a.Function;
  });

  it('should return a number', function() {
    var result = arrayception([ [ 5 ], [ [ ] ] ]);
    should.exist(result);
    result.should.be.an.instanceof(Number);
  });


  it('should do the thing', function(){
    var result = arrayception([ [ 5 ], [ [ ] ] ]);
    result.should.be.eql(2);
  });

    it('should do the damn thing', function(){
    var result = arrayception([ 10, 20, 30, 40 ]);
    result.should.be.eql(1);
  });

      it('should keep doing the thing', function(){
    var result = arrayception([ [ 10, 20 ], [ [ 30, [ 40 ] ] ] ]);
    result.should.be.eql(4);
  });

        it('should always do the thing', function(){
    var result = arrayception([0]);
    result.should.be.eql(0);
  });

        it('should never stop doing the thing', function(){
    var result = arrayception([ [ [ ] ] ]);
    result.should.be.eql(0);
  });

  });

