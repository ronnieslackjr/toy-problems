var should = chai.should();

describe('fractionConverter', function() {
  it('should exist', function(){
    should.exist(fractionConverter);
  });

  it('should be a function', function() {
    fractionConverter.should.be.a.Function;
  });

  it ('should convert a 0.5 to 1/2', function(){
    const actual = fractionConverter(0.5)
    const expected = '1/2'
    expect(actual).to.equal(expected);
  })

    it ('should convert a 3 to 3/1', function(){
    const actual = fractionConverter(3)
    const expected = '3/1'
    expect(actual).to.equal(expected);
  })
  it ('should convert a 2.75 to 11/4', function(){
    const actual = fractionConverter(2.75)
    const expected = '11/4'
    expect(actual).to.equal(expected);
  }) 


  
});
console.log(fractionConverter(2.44))
console.log(fractionConverter(7.62))
console.log(fractionConverter(3.33))

